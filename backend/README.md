## Description

Social Feed test MVP application backend.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# start mongodb with docker, mongodb default port is used, if you want to use another port
# please update it in .env file
$ docker-compose up

# run app on dev mode
$ npm run start
```
