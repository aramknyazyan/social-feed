export const CATEGORIES = [
  {
    _id: 'cat1',
    name: 'Category 1',
  },
  {
    _id: 'cat2',
    name: 'Category 2',
  },
  {
    _id: 'cat3',
    name: 'Category 3',
  },
  {
    _id: 'cat4',
    name: 'Category 4',
  },
  {
    _id: 'cat5',
    name: 'Category 5',
  },
];
