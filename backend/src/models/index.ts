import { UserModel } from './user.model';
import { PostModel } from './post.model';

export const Models = [UserModel, PostModel];
