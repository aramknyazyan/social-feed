import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { User } from './user.model';

export type PostDocument = Post & Document;

@Schema()
export class Post {
  @Prop({ required: true })
  subject: string;

  @Prop({ required: true })
  message: string;

  @Prop({ required: true })
  category: string;

  @Prop({ type: Types.ObjectId, ref: User.name })
  user: User;

  @Prop()
  favorites: string[];

  @Prop({ default: 0 })
  popularity: number;

  @Prop()
  image: string;

  @Prop({ default: Date.now })
  date: Date;
}

const PostSchema = SchemaFactory.createForClass(Post);

export const PostModel = { name: Post.name, schema: PostSchema };
