import { IsString, IsNotEmpty, IsDefined } from 'class-validator';

export class CreatePostRequest {
  @IsDefined()
  @IsNotEmpty()
  @IsString()
  subject: string;

  @IsDefined()
  @IsNotEmpty()
  @IsString()
  message: string;

  @IsDefined()
  @IsNotEmpty()
  @IsString()
  category: string;
}
