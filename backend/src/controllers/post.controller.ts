import {
  Controller,
  Get,
  UseGuards,
  Post,
  Patch,
  Delete,
  Req,
  Query,
  Body,
  UseInterceptors,
  UploadedFile,
  Param,
} from '@nestjs/common';
import { PostService } from '../services';
import { AccessControlGuard } from '../common/auth';
import { CreatePostRequest, GetPostsRequest } from '../request-models';
import { FileInterceptor } from '@nestjs/platform-express';

@UseGuards(AccessControlGuard)
@Controller('/api/posts')
export class PostController {
  constructor(private readonly postService: PostService) {}

  @Get()
  async getPosts(@Query() query: GetPostsRequest): Promise<any> {
    return await this.postService.getPosts(query);
  }

  @Post()
  async createPost(@Req() req, @Body() data: CreatePostRequest): Promise<any> {
    const user = req.user.userId;
    return await this.postService.createPost({ ...data, user });
  }

  @Post('/:id/image')
  @UseInterceptors(FileInterceptor('image'))
  async uploadedFile(@Req() req, @UploadedFile() file, @Param() { id }) {
    const image = file.filename;
    return await this.postService.updatePost(id, { image });
  }

  @Delete('/:id')
  async deletePost(id): Promise<any> {
    return await this.postService.deletePost(id);
  }

  @Patch('/:id/favorite')
  async updatePostFavorite(
    @Req() req,
    @Param() { id },
    @Body() { isFavorite },
  ): Promise<any> {
    return await this.postService.updatePostFavorite(
      id,
      req.user.userId,
      isFavorite,
    );
  }
}
