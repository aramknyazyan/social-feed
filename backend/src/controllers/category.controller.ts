import { Controller, Get, UseGuards } from '@nestjs/common';
import { AccessControlGuard } from '../common/auth';
import { CATEGORIES } from '../constants';

@UseGuards(AccessControlGuard)
@Controller('/api/categories')
export class CategoryController {
  constructor() {}

  @Get()
  async getCategories(): Promise<any> {
    return CATEGORIES;
  }
}
