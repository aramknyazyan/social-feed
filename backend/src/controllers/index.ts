import { AuthController } from './auth.controller';
import { UserController } from './user.controller';
import { PostController } from './post.controller';
import { CategoryController } from './category.controller';

export const Controllers = [
  AuthController,
  UserController,
  PostController,
  CategoryController,
];
