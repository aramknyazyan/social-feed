import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { omit } from 'lodash';
import { Post, PostDocument } from '../models/post.model';

@Injectable()
export class PostService {
  constructor(@InjectModel(Post.name) private postModel: Model<PostDocument>) {}

  async createPost(data): Promise<Post> {
    const post = new this.postModel(data);
    return await post.save();
  }

  async getPosts(query): Promise<Post[]> {
    const { sortBy } = query;
    const sort = {};
    sort[sortBy] = -1;
    const filter = omit(query, 'sortBy');
    return this.postModel.find(filter).populate('user').sort(sort);
  }

  async updatePost(id, data): Promise<any> {
    const post = await this.postModel.findById(id);
    Object.assign(post, data);
    await post.save();
    return this.postModel.findById(post._id).populate('user');
  }

  async deletePost(id): Promise<any> {
    return this.postModel.deleteOne(id);
  }

  async updatePostFavorite(postId, userId, isFavorite): Promise<Post> {
    const post = await this.postModel.findById(postId);
    if (!isFavorite) {
      post.favorites.push(userId);
      post.popularity++;
    } else {
      post.favorites = post.favorites.filter((fv) => fv !== userId);
      post.popularity--;
    }
    await post.save();
    return post;
  }
}
