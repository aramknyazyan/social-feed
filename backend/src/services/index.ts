export * from './auth.service';
export * from './user.service';
export * from './post.service';

import { AuthService } from './auth.service';
import { UserService } from './user.service';
import { PostService } from './post.service';

export const Services = [AuthService, UserService, PostService];
