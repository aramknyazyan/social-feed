# README

Social Feed test MVP APP

In order to start the applications please check README files on backend and frontend folders.
Please note that React.js is used on frontend application and Node.js, Nest.js, MongoDB are used for backend application.
There is a docker-compose.yml to start mongodb

Social Feed MVP APP has the following functional:

- User Registration/Authentication
- After login user can create a post by selecting category (categories list is hardcoded in backend, in real app it will be in DB).
- In Posts page he will see the list of all posts, which can be filtered by category and sorted by popularity and date.
- On each post item the author name and avatar are presented and on clickin to it current user page is opened
- On user page you can see user details, user posts, user favorite posts
- If user is the owner of post, delete icon will be on post, else like/favorite icon
- My Account page is the same user page but here user can edit his profile data by clicking on edit icon below

Please note that the product is an MVP, several things are not handled both on backend and frontend.
In case the reviewer has questions we can schedule a call and discuss the details.
