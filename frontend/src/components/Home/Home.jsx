import React from 'react';
import Sidebar from '../common/Sidebar';
import Posts from '../common/Posts';
import { Col, Row } from 'antd';

const Home = () => {
  return (
    <Sidebar defaultSelectedKey="1">
      <Row>
        <Col offset={8}>
          <Posts />
        </Col>
      </Row>
    </Sidebar>
  );
};

export default Home;
