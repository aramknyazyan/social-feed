import React, { useState } from 'react';
import { Button, Form, Input, Modal, notification, Select } from 'antd';
import UploadImage from '../common/UploadImage';
import { createPost, uploadPostImage } from '../api';

const { Option } = Select;

const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

const PostCreateModal = ({
  modalVisible,
  setModalVisible,
  categories,
  posts,
  setPosts,
}) => {
  const [image, setImage] = useState(null);
  const [imageError, setImageError] = useState('');

  const handleSubmit = async (values) => {
    if (!image) {
      setImageError('Please upload an image');
      return;
    }
    try {
      let { data } = await createPost(values);
      const uploadResult = await uploadPostImage(data._id, image);
      setPosts([uploadResult.data, ...posts]);
      setModalVisible(false);
    } catch (error) {
      const message = error.response.data.message;
      notification.error({ message });
    }
  };

  return (
    <Modal
      title="Create Post"
      visible={modalVisible}
      onCancel={() => setModalVisible(false)}
      footer={null}
    >
      <Form {...layout} name="basic" onFinish={handleSubmit}>
        <Form.Item
          label="Category"
          name="category"
          rules={[
            {
              required: true,
              message: 'Please select category',
            },
          ]}
        >
          <Select placeholder="Select Category">
            {categories.map((cat) => {
              return (
                <Option key={cat._id} value={cat._id}>
                  {cat.name}
                </Option>
              );
            })}
          </Select>
        </Form.Item>

        <Form.Item
          label="Subject"
          name="subject"
          rules={[
            {
              required: true,
              message: 'Please provide post subject',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Message"
          name="message"
          rules={[
            {
              required: true,
              message: 'Please provide post message',
            },
          ]}
        >
          <Input.TextArea />
        </Form.Item>

        <Form.Item {...tailLayout}>
          <UploadImage setImage={setImage} />
          {!image && <span style={{ color: 'red' }}>{imageError}</span>}
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default PostCreateModal;
