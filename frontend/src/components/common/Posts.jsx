import React, { useState, useEffect } from 'react';
import { Spin, Button, notification, Select, Row, Col } from 'antd';
import PostCreateModal from './PostCreateModal';
import Post from './Post';
import {
  getPosts,
  updatePostFavorite,
  deletePost,
  getCategories,
} from '../api';

const { Option } = Select;

const Posts = ({ user = null, favorites = false }) => {
  const myUser = JSON.parse(localStorage.getItem('user'));
  const { userId: myUserId } = myUser;
  const [loading, setLoading] = useState(true);
  const [modalVisible, setModalVisible] = useState(false);
  const [sortBy, setSortBy] = useState('date');
  const [posts, setPosts] = useState([]);
  const [category, setCategory] = useState('all');
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    const fetchPosts = async () => {
      const catFilter = category === 'all' ? null : category;
      const result = await getPosts(
        { user, favorites, category: catFilter },
        sortBy,
      );
      setPosts(result.data);
      setLoading(false);
    };
    fetchPosts();
  }, [user, posts, favorites, category, sortBy]);

  useEffect(() => {
    const fetchCategories = async () => {
      const result = await getCategories();
      setCategories(result.data);
    };
    fetchCategories();
  }, []);

  const handleFavorite = async (postId, isFavorite) => {
    try {
      let { data } = await updatePostFavorite(postId, isFavorite);
      const newPosts = posts.map((post) => {
        if (post._id === data._id) {
          post.favorites = data.favorites;
        }
        return post;
      });
      setPosts(newPosts);
    } catch (error) {
      const message = error.response.data.message;
      notification.error({ message });
    }
  };

  const handleDelete = async (postId) => {
    try {
      await deletePost(postId);
      const newPosts = posts.filter((post) => post._id !== postId);
      setPosts(newPosts);
    } catch (error) {
      const message = error.response.data.message;
      notification.error({ message });
    }
  };

  const handleSort = (sort) => {
    setSortBy(sort);
  };

  const handleCategoryChange = (cat) => {
    setCategory(cat);
  };

  return (
    <Spin spinning={loading}>
      {!user && !favorites && (
        <Button type="primary" block onClick={() => setModalVisible(true)}>
          Add Post
        </Button>
      )}
      <Row>
        <Col span={12}>
          <Select
            value={sortBy}
            onSelect={handleSort}
            style={{ width: '180px', margin: '10px' }}
          >
            <Option value="date" selected>
              Sort by Newness
            </Option>
            <Option value="popularity">Sort by Popularity</Option>
          </Select>
        </Col>
        <Col span={12}>
          {!user && !favorites && (
            <Select
              value={category}
              onSelect={handleCategoryChange}
              style={{ width: '180px', margin: '10px' }}
            >
              <Option value="all" selected>
                All
              </Option>
              {categories.map((cat) => {
                return (
                  <Option key={cat._id} value={cat._id}>
                    {cat.name}
                  </Option>
                );
              })}
            </Select>
          )}
        </Col>
      </Row>

      <PostCreateModal
        modalVisible={modalVisible}
        posts={posts}
        categories={categories}
        setModalVisible={setModalVisible}
        setPosts={setPosts}
      />
      {posts.map((post) => {
        return (
          <Post
            key={post._id}
            post={post}
            myUserId={myUserId}
            onFavorite={handleFavorite}
            onDelete={handleDelete}
          />
        );
      })}
    </Spin>
  );
};

export default Posts;
