import React from 'react';
import * as moment from 'moment';
import { Card, Avatar } from 'antd';
import { LikeOutlined, LikeFilled, DeleteOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';
import { config } from '../../configs';

const { Meta } = Card;

const Post = ({ post, myUserId, onFavorite, onDelete }) => {
  const isFavorite = post.favorites.includes(myUserId);
  const popularity = post.popularity;
  const FavoriteIcon = isFavorite ? LikeFilled : LikeOutlined;
  const FavoriteActions =
    myUserId === post.user._id
      ? [
          <DeleteOutlined key="delete" onClick={() => onDelete(post._id)} />,
          popularity,
        ]
      : [
          <FavoriteIcon
            key="like"
            onClick={() => onFavorite(post._id, isFavorite)}
          />,
          popularity,
        ];

  const getFormattedDate = (date) => {
    return moment(date).format('MMM, DD hh:mm A');
  };

  return (
    <Card
      style={{ width: 400, margin: '10px auto' }}
      title={
        <Link to={`/users/${post.user._id}`}>
          <Meta
            avatar={
              <Avatar src={`${config.APP_API_ENDPOINT}/${post.user.image}`} />
            }
            title={`${post.user.firstName} ${post.user.lastName}`}
            extra={post.user.email}
          />
        </Link>
      }
      extra={<span>{getFormattedDate(post.date)}</span>}
      cover={
        <img
          alt="example"
          src={`${config.APP_API_ENDPOINT}/${post.image}`}
          style={{ maxHeight: '200px' }}
        />
      }
      actions={FavoriteActions}
    >
      <Meta title={post.subject} description={post.message} />
    </Card>
  );
};

export default Post;
