import axios from 'axios';
import { pickBy } from 'lodash';
import {
  authenticationEndpoint,
  registerEndpoint,
  getUsersEndpoint,
  getPostsEndpoint,
  getCategoriesEndpoint,
} from '../configs';

let headers,
  multiPartHeaders = {};
const userData = localStorage.getItem('user');

if (userData) {
  const user = JSON.parse(userData);
  const { token } = user;
  headers = {
    headers: { Authorization: `Bearer ${token}` },
  };
  multiPartHeaders = {
    headers: {
      Authorization: `Bearer ${token}`,
      'Content-Type': 'multipart/form-data',
    },
  };
}

export const authenticate = async (data) => {
  return axios.post(authenticationEndpoint(), data);
};

export const register = async (data) => {
  return axios.post(registerEndpoint(), data);
};

export const getUser = async (id) => {
  return axios.get(getUsersEndpoint(id), headers);
};

export const updateUser = async (id, data) => {
  return axios.put(getUsersEndpoint(id), data, headers);
};

export const uploadUserImage = async (id, data, passedHeaders = null) => {
  const uploadHeaders = passedHeaders || multiPartHeaders;
  return axios.post(`${getUsersEndpoint(id)}/image`, data, uploadHeaders);
};

export const createPost = async (data) => {
  return axios.post(getPostsEndpoint(), data, headers);
};

export const uploadPostImage = async (id, data) => {
  return axios.post(`${getPostsEndpoint(id)}/image`, data, multiPartHeaders);
};

export const getPosts = async (params, sortBy) => {
  const notNullParams = pickBy(params);
  const query = Object.keys(notNullParams)
    .map((key) => `${key}=${params[key].toString()}`)
    .join('&');
  const url = `${getPostsEndpoint()}?${query}&sortBy=${sortBy}`;
  return axios.get(url, headers);
};

export const updatePostFavorite = async (id, isFavorite) => {
  return axios.patch(
    `${getPostsEndpoint(id)}/favorite`,
    { isFavorite },
    headers,
  );
};

export const deletePost = async (id) => {
  return axios.delete(getPostsEndpoint(id), headers);
};

export const getCategories = async () => {
  return axios.get(getCategoriesEndpoint(), headers);
};
