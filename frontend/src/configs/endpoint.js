import { config } from '.';

export const authenticationEndpoint = () => {
  return `${config.APP_API_ENDPOINT}/api/authenticate`;
};

export const registerEndpoint = () => {
  return `${config.APP_API_ENDPOINT}/api/register`;
};

export const getUsersEndpoint = (id = null) => {
  return id
    ? `${config.APP_API_ENDPOINT}/api/users/${id}`
    : `${config.APP_API_ENDPOINT}/api/users`;
};

export const getPostsEndpoint = (id = null) => {
  return id
    ? `${config.APP_API_ENDPOINT}/api/posts/${id}`
    : `${config.APP_API_ENDPOINT}/api/posts`;
};

export const getCategoriesEndpoint = () => {
  return `${config.APP_API_ENDPOINT}/api/categories`;
};
